const API = 'https://ajax.test-danit.com/api/json/'
const postsWrapper = document.createElement('div')
class Card {
    constructor({title, body, name, email, id}) {
        this.title = title;
        this.description = body;
        this.name = name;
        this.email = email;
        this.id = id;
    }
    render() {
        postsWrapper.insertAdjacentHTML('beforeend', `
        <div class="user-post" id=${this.id}>
        Title: ${this.title}<br>
        Text: ${this.description}<br>
        Author: ${this.name}<br>
        Email: ${this.email}<br>
        <button>Delete</button>
        `)
        const postId = document.getElementById(`${this.id}`)
        const deleteBtn = postId.querySelector('button').onclick = this.deletePost
    }
    deletePost = async () => {
        const postId = document.getElementById(`${this.id}`)
        const deleteAPI = `https://ajax.test-danit.com/api/json/posts/${this.id}`
        const deletePost = await sendRequest(deleteAPI, 'DELETE')
        if (deletePost.ok) {
            postId.remove()
        }
    }
}
async function sendRequest(url, method = "GET", options){
    const response= await fetch(url, {
        method: method,
        ...options,
    })
    if (response.ok) {
        if (method !== "DELETE") {
            const result = response.json()
            return result
        } else {
            return response
        }
    } else {
        return new Error("Error")
    }
}
async function getUsers() {
    const users = await sendRequest(`${API}users`)
    users.forEach((user) => {
        async function getPosts(){
            const posts = await sendRequest(`${API}users/${user.id}/posts`)
            console.log(posts);
            posts.forEach((post) => {
                const userPost = new Card(({...user, ...post}))
                userPost.render(postsWrapper)
            })
        }
        getPosts()
    })
}
getUsers()

document.body.append(postsWrapper)


// fetch(urlUsers)
//     .then(response => response.json())
//     .then((data) => {
//         const users = data
//         fetch(urlPosts)
//             .then(response => response.json())
//             .then((result) => {
//                 const posts = result;
//                 users.forEach((user) => {
//                     posts.forEach((post) => {
//                         if (user.id === post.userId) {
//                             const card = new Card(post.title, post.body, user.name, user.email, post.id)
//                             card.render(postsWrapper)
//                         }
//                     })
//                 })
//             })
//     })
//
// document.body.append(postsWrapper)