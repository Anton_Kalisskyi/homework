import {getAllCards} from "./getAllCards.js";
import {publishCard} from "./publishCard.js";
export function createVisit() {
    const token = localStorage.getItem('token')
    fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            doctor: this.doctorSelect.value,
            name: this.visitorName.value,
            title: this.visitGoal.value,
            description: this.visitDescription.value,
            urgency: this.visitUrgency.value,
            bp: this.visitorBloodPressure.value,
            bmi: this.visitorBodyMassIndex.value,
            diseases: this.visitorPastCardioDiseases.value,
            age: this.visitorAge.value,
            lastDateVisit: this.lastVisitDate.value,
            // status: this.status.value
        })
    })
        .then(response => response.json())
        .then(response => publishCard(response)
        )
}