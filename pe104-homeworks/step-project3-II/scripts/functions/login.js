import {getToken} from "./getToken.js";

export function login(url) {
    const userEmail = document.querySelector('#email').value
    const userPassword = document.querySelector('#password').value

    if(userEmail.includes('@') && userEmail.includes('.') && userPassword.length >= 3) {
        getToken(url, userEmail, userPassword)
    } else {
        alert("Please, enter correct email and password")
        const inputs = Array.from(document.querySelectorAll('input'))
        inputs.forEach((input) => {
            input.style.color = 'red'
        })
    }
}