import {Confirm} from "../classes/Modal.js";
export function deleteCardConfirm(card) {
    const confirmVisitDelete = new Confirm();
    confirmVisitDelete.renderDeleteConfirm(card);
}