export function deleteCard(card, userToken){
    fetch(`https://ajax.test-danit.com/api/v2/cards/${card.id}`, {
    method: 'DELETE',
    headers: {
        'Authorization': `Bearer ${userToken}`
    }
    })
        .then(response => {
            if(response.ok) {
                // this.closeModal()
                card.visitWrapper.remove()
            } else {
                card.visitWrapper.remove()
            }
        })
}