import {publishCard} from "./publishCard.js";
export function getAllCards(token) {
    const response = fetch('https://ajax.test-danit.com/api/v2/cards', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    })
        .then(response => response.json())
        .then(cards => {
            cards.forEach((card) => {
                publishCard(card)
            })
        })
}