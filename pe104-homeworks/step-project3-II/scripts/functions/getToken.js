import {loginSuccess} from "./loginSuccess.js";
export async function getToken(url, userEmail, userPassword) {
    await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({email: userEmail, password: userPassword})
    })
        .then(response => response.text())
        .then(token => {
            if(token === "Incorrect username or password") {
                alert("Incorrect username or password. Try again.")
                const inputs = Array.from(document.querySelectorAll('input'))
                inputs.forEach((input) => {
                    input.style.color = 'red'})
            } else {
                loginSuccess(token);
                return token
            }
        })
}
