import {Visit} from "./Visit.js";

export class VisitCardiologist extends Visit {
    constructor(doctor, name, title, description, urgency, bp, bmi, diseases, age, status, id) {
        super(doctor, name, title, description, urgency, status, id);
        this.bp = bp
        this.bmi = bmi
        this.diseases = diseases
        this.age = age
        this.bpField = document.createElement('p')
        this.bmiField = document.createElement('p')
        this.diseasesField = document.createElement('p')
        this.ageField = document.createElement('p')
    }
    renderVisitCardiologist(container) {
        super.renderVisit();
        container.append(this.visitWrapper)
    }
    showMore(container) {
        super.showMore()
        this.bpField.innerHTML = `<span>Blood Pressure: </span> ${this.bp}`
        this.bmiField.innerHTML = `<span>Blood Pressure: </span> ${this.bmi}`
        this.diseasesField.innerHTML = `<span>Past Cardio Diseases: </span> ${this.diseases}`
        this.ageField.innerHTML = `<span>Visitor's age: </span> ${this.age}`
        this.visitFieldsWrapper.append(this.bpField, this.bmiField, this.diseasesField, this.ageField)
    }
    showLess() {
        super.showLess();
        this.bpField.remove()
        this.bmiField.remove()
        this.diseasesField.remove()
        this.ageField.remove()
    }
}