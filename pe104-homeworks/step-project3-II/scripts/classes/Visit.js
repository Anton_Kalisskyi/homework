import {deleteCardConfirm} from "../functions/deleteCardConfirm.js";
import {editCard} from "../functions/editCard.js";
export class Visit{
    constructor(doctor, name, title, description, urgency, status = 'Open', id) {
        this.doctor = doctor
        this.name = name
        this.title = title
        this.description = description
        this.urgency = urgency
        this.status = status
        this.id = id
        this.visitWrapper = document.createElement('div')
        this.visitTitleWrapper = document.createElement('div')
        this.visitFieldsWrapper = document.createElement('div')
        this.doctorField = document.createElement('p')
        this.nameField = document.createElement('p')
        this.titleField = document.createElement('p')
        this.descriptionField = document.createElement('p')
        this.urgencyField = document.createElement('p')
        this.visitStatusField = document.createElement('p')
        this.visitIdField = document.createElement('p')
        this.showMoreBtn = document.createElement('button')
        this.showLessBtn = document.createElement('button')
        this.visitEditButton = document.createElement('button')
        this.visitDeleteButton = document.createElement('div')
    }
    renderVisit() {
        this.doctorField.innerHTML = `<span>Doctor: </span> ${this.doctor}`
        this.nameField.innerHTML = `<span>Visitor: </span> ${this.name}`
        this.titleField.innerHTML = `<span>Purpose: </span> ${this.title}`
        this.descriptionField.innerHTML = `<span>Description: </span> ${this.description}`
        this.urgencyField.innerHTML = `<span>Urgency: </span> ${this.urgency}`
        this.visitStatusField.innerHTML = `<span>Status: </span> ${this.status}`
        this.visitIdField.innerHTML = `<span>Visit Id: </span> ${this.id}`
        this.visitWrapper.classList.add('visit-wrapper')
        this.visitTitleWrapper.classList.add('visit-buttons-wrapper')
        this.visitFieldsWrapper.classList.add('visit-fields-wrapper')
        this.visitDeleteButton.classList.add('visit-delete-btn')
        this.visitDeleteButton.addEventListener('click', (event) => {
            event.preventDefault();
            this.deleteVisit()
        })
        this.showLessBtn.classList.add('button-less')
        this.showLessBtn.classList.add('hidden')
        this.showLessBtn.innerText = 'Less'
        this.showLessBtn.addEventListener('click', (event) => {
            event.preventDefault();
            this.showLessBtn.classList.toggle('hidden')
            this.showMoreBtn.classList.toggle('hidden')
            this.showLess()
        })
        this.showMoreBtn.classList.add('button-more')
        this.showMoreBtn.innerText = 'More'
        this.showMoreBtn.addEventListener('click', (event) => {
            event.preventDefault();
            this.showLessBtn.classList.toggle('hidden')
            this.showMoreBtn.classList.toggle('hidden')
            this.showMore()
        })
        this.visitEditButton.classList.add('button-edit')
        this.visitEditButton.innerText = 'Edit'
        this.visitEditButton.addEventListener('click', (event) => {
            event.preventDefault();
            this.editVisit()
        })
        this.visitTitleWrapper.append(
            this.visitEditButton,
            this.showMoreBtn,
            this.showLessBtn,
            this.visitDeleteButton
        )
        this.visitFieldsWrapper.append(this.doctorField, this.nameField)
        this.visitWrapper.append(this.visitTitleWrapper, this.visitFieldsWrapper)
    }
    showMore() {
        this.visitFieldsWrapper.append(
            this.visitStatusField,
            this.titleField,
            this.descriptionField,
            this.urgencyField,
            this.visitIdField
        )
    }
    showLess() {
            this.visitStatusField.remove()
            this.titleField.remove()
            this.descriptionField.remove()
            this.urgencyField.remove()
            this.visitIdField.remove()
    }
    editVisit() {
        editCard(this)
    }
    deleteVisit() {
        deleteCardConfirm(this)
    }
}