import {Visit} from "./Visit.js";

export class VisitTherapist extends Visit{
    constructor(doctor, name, title, description, urgency, age, status, id) {
        super(doctor, name, title, description, urgency, status, id);
        this.age = age;
        this.ageField = document.createElement('p')
    }
    renderVisitTherapist(container) {
        super.renderVisit()
        container.append(this.visitWrapper)
    }
    showMore(container) {
        super.showMore()
        this.ageField.innerHTML = `<span>Visitor's age: </span> ${this.age}`
        this.visitFieldsWrapper.append(this.ageField)
    }
    showLess() {
        super.showLess();
        this.ageField.remove()
    }
}