import {API, doctorsList, urgencyList} from "../constants/constants.js";
import {login} from "../functions/login.js";
import {deleteCard} from "../functions/deleteCard.js";

const userToken = localStorage.getItem('token');
export class Modal{
    constructor() {
        this.modalWrapper = document.createElement('div')
        this.modalWrapperBackground = document.createElement('div')
        this.modalBody = document.createElement('form')
        this.modalTitleWrapper = document.createElement('div')
        this.modalTitle = document.createElement('h3')
        this.modalCloseButton = document.createElement('div')
    }
    renderModal() {
        this.modalWrapper.classList.add('modal-wrapper')
        this.modalWrapperBackground.classList.add('modal-wrapper-bgd')
        this.modalWrapperBackground.addEventListener('click', this.closeModal.bind(this))
        this.modalBody.classList.add('modal-body')
        this.modalTitleWrapper.classList.add('modal-title-wrapper')
        this.modalTitle.classList.add('modal-title')
        this.modalCloseButton.classList.add('modal-close-btn')
        this.modalCloseButton.addEventListener('click', this.closeModal.bind(this))
        this.modalTitleWrapper.append(this.modalTitle, this.modalCloseButton);
        this.modalWrapper.append(this.modalBody, this.modalWrapperBackground);
        this.modalBody.append(this.modalTitleWrapper);
    }
    closeModal() {
        this.modalWrapper.remove()
    }
}
export class ModalLogIn extends Modal{
    constructor() {
        super();
        this.modalLogInEmailWrapper = document.createElement('div')
        this.modalLogInPasswordWrapper = document.createElement('div')
        this.modalLogInEmailLabel = document.createElement('label')
        this.modalLogInEmail = document.createElement('input')
        this.modalLogInPasswordLabel = document.createElement('label')
        this.modalLogInPassword = document.createElement('input')
        this.modalLogInSubmitBtn = document.createElement('button')
    }
    renderModalLogIn(){
        super.renderModal();
        this.modalTitle.innerText = 'Authorization';
        this.modalLogInEmailWrapper.classList.add('modal-input-wrapper')
        this.modalLogInEmail.classList.add('modal-log-in-email')
        this.modalLogInEmail.id = 'email'
        this.modalLogInEmail.required = true;
        this.modalLogInEmailLabel.innerText = 'Email*'
        this.modalLogInPasswordWrapper.classList.add('modal-input-wrapper')
        this.modalLogInPassword.classList.add('modal-log-in-password')
        this.modalLogInPassword.id = 'password'
        this.modalLogInPassword.type = 'password'
        this.modalLogInPassword.required = true;
        this.modalLogInEmail.placeholder = 'Enter your email'
        this.modalLogInPassword.placeholder = 'Enter your password'
        this.modalLogInPasswordLabel.innerText = 'Password*'
        this.modalLogInSubmitBtn.classList.add('modal-btn')
        this.modalLogInSubmitBtn.type = 'submit'
        this.modalLogInSubmitBtn.innerText = 'Submit'
        this.modalLogInSubmitBtn.addEventListener('click', (event) => {
            event.preventDefault();
            login(API)
        })
        this.modalLogInEmailWrapper.append(this.modalLogInEmailLabel, this.modalLogInEmail)
        this.modalLogInPasswordWrapper.append(this.modalLogInPasswordLabel, this.modalLogInPassword)
        this.modalBody.append(this.modalLogInEmailWrapper, this.modalLogInPasswordWrapper, this.modalLogInSubmitBtn)
        document.body.append(this.modalWrapper);
    }

}

export class ChooseDoctor extends Modal {
    constructor(creatVisit) {
        super();
        this.creatVisit = creatVisit
        this.doctorSelect = document.createElement('select')
        this.visitWrapper = document.createElement('div')
        this.doctorFieldsWrapper = document.createElement('div')
        this.visitGoalWrapper = document.createElement('div')
        this.visitGoalLabel = document.createElement('label')
        this.visitGoal = document.createElement('input')
        this.visitDescriptionWrapper = document.createElement('div')
        this.visitDescriptionLabel = document.createElement('label')
        this.visitDescription = document.createElement('textarea')
        this.visitUrgencyWrapper = document.createElement('div')
        this.visitUrgencyLabel = document.createElement('label')
        this.visitUrgency = document.createElement('select')
        this.visitorNameWrapper = document.createElement('div')
        this.visitorNameLabel = document.createElement('label')
        this.visitorName = document.createElement('input')
        this.visitorBloodPressureWrapper = document.createElement('div')
        this.visitorBloodPressureLabel = document.createElement('label')
        this.visitorBloodPressure = document.createElement('input')
        this.visitorBodyMassIndexWrapper = document.createElement('div')
        this.visitorBodyMassIndexLabel = document.createElement('label')
        this.visitorBodyMassIndex = document.createElement('input')
        this.visitorPastCardioDiseasesWrapper = document.createElement('div')
        this.visitorPastCardioDiseasesLabel = document.createElement('label')
        this.visitorPastCardioDiseases = document.createElement('input')
        this.visitorAgeWrapper = document.createElement('div')
        this.visitorAgeLabel = document.createElement('label')
        this.visitorAge = document.createElement('input')
        this.lastVisitDateWrapper = document.createElement('div')
        this.lastVisitDateLabel = document.createElement('label')
        this.lastVisitDate = document.createElement('input')
        this.modalCreatVisitBtn = document.createElement('button')
    }
    renderDoctorFields(){
        super.renderModal();
        this.modalTitle.innerText = 'Create new visit';
        this.visitGoal.required = true;
        this.visitDescription.required = true;
        this.visitorName.required = true;
        this.modalCreatVisitBtn.innerText = 'Create'
        this.modalCreatVisitBtn.classList.add('modal-btn')
        this.doctorSelect.classList.add('doctor-select')
        this.modalCreatVisitBtn.addEventListener('click', (event) => {
            event.preventDefault()
            this.creatVisit()
        })
        this.modalCreatVisitBtn.addEventListener('click', this.closeModal.bind(this))
        doctorsList.forEach((speciality) => {
            const profession = document.createElement('option')
            profession.value = speciality
            profession.textContent = speciality
            this.doctorSelect.append(profession)
        })
        this.doctorSelect.addEventListener('change', (event) => {
            if (event.target.value === "Cardiologist") {
                this.doctorFieldsWrapper.innerHTML = null;
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitorNameWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitGoalWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitDescriptionWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitUrgencyWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitorBloodPressureWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitorBodyMassIndexWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitorPastCardioDiseasesWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitorAgeWrapper)
                this.modalBody.insertAdjacentElement("beforeend", this.modalCreatVisitBtn)
            } else if(event.target.value === "Dentist") {
                this.doctorFieldsWrapper.innerHTML = null;
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitorNameWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitGoalWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitDescriptionWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitUrgencyWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.lastVisitDateWrapper)
                this.modalBody.insertAdjacentElement("beforeend", this.modalCreatVisitBtn)
            } else if(event.target.value === "Therapist") {
                this.doctorFieldsWrapper.innerHTML = null;
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitorNameWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitGoalWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitDescriptionWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitUrgencyWrapper)
                this.doctorFieldsWrapper.insertAdjacentElement('beforeend', this.visitorAgeWrapper)
                this.modalBody.insertAdjacentElement("beforeend", this.modalCreatVisitBtn)
                } else {
                this.doctorFieldsWrapper.innerHTML = null;
            }
        })
        this.modalBody.append(this.doctorSelect)
        urgencyList.forEach((option) => {
            const urgencyRate = document.createElement('option')
            urgencyRate.value = option
            urgencyRate.textContent = option
            this.visitUrgency.append(urgencyRate)
        })
        this.visitGoalLabel.innerText = "Visit goal*"
        this.visitDescriptionLabel.innerText = "Visit description*"
        this.visitUrgencyLabel.innerText = "Urgency*"
        this.visitorNameLabel.innerText = "Visitor's full name*"
        this.doctorFieldsWrapper.classList.add('modal-fields-wrapper')
        this.visitorBloodPressureLabel.innerText = "Visitor's blood pressure"
        this.visitorBodyMassIndexLabel.innerText = "Visitor's Body Mass Index"
        this.visitorPastCardioDiseasesLabel.innerText = "Visitor's past cardio diseases"
        this.visitorAgeLabel.innerText = "Visitor's age"
        this.lastVisitDateLabel.innerText = "Last visit date"
        this.lastVisitDate.type = 'date'
        this.visitGoalWrapper.append(this.visitGoalLabel, this.visitGoal)
        this.visitDescriptionWrapper.append(this.visitDescriptionLabel, this.visitDescription)
        this.visitUrgencyWrapper.append(this.visitUrgencyLabel, this.visitUrgency)
        this.visitorNameWrapper.append(this.visitorNameLabel, this.visitorName)
        this.visitorBloodPressureWrapper.append(this.visitorBloodPressureLabel, this.visitorBloodPressure)
        this.visitorBodyMassIndexWrapper.append(this.visitorBodyMassIndexLabel, this.visitorBodyMassIndex)
        this.visitorPastCardioDiseasesWrapper.append(this.visitorPastCardioDiseasesLabel, this.visitorPastCardioDiseases)
        this.visitorAgeWrapper.append(this.visitorAgeLabel, this.visitorAge)
        this.lastVisitDateWrapper.append(this.lastVisitDateLabel, this.lastVisitDate)
        this.visitWrapper.classList.add('modal-visit-wrapper')
        this.visitWrapper.append(this.doctorFieldsWrapper)
        this.modalBody.append(this.visitWrapper)
        document.body.append(this.modalWrapper);
    }
}
export class Confirm extends Modal{
    constructor() {
        super()
        this.buttonsWrapper = document.createElement('div')
        this.confirmBtn = document.createElement('button')
        this.rejectBtn = document.createElement('button')
    }
    renderDeleteConfirm(card){
        super.renderModal();
        this.modalTitle.innerText = 'Do you really want to delete this Card?'
        this.confirmBtn.type = 'submit'
        this.confirmBtn.classList.add('button')
        this.confirmBtn.classList.add('confirm-delete-btn')
        this.confirmBtn.innerText = 'Delete'
        this.confirmBtn.addEventListener('click', (event) => {
            event.preventDefault()
            this.modalWrapper.remove()
            deleteCard(card, userToken)
        })
        this.confirmBtn.addEventListener('click', this.closeModal.bind(this))
        this.rejectBtn.type = 'button'
        this.rejectBtn.classList.add('button')
        this.rejectBtn.classList.add('reject-delete-btn')
        this.rejectBtn.innerText = 'Reject'
        this.rejectBtn.addEventListener('click', this.closeModal.bind(this))
        this.buttonsWrapper.classList.add('buttons-confirm-wrapper')
        this.buttonsWrapper.append(this.confirmBtn, this.rejectBtn)
        this.modalBody.append(this.buttonsWrapper)
        document.body.append(this.modalWrapper);
    }
}



export class Edit extends ChooseDoctor{
    constructor() {
        super();
    }
    renderEdit(cardInfo){
        super.renderModal();
        if(cardInfo.doctor === 'Therapist') {
            console.log(cardInfo.doctor);
        }
        // this.renderDoctorFields();

        this.modalTitle.innerText = 'Edit card'

        document.body.append(this.modalWrapper);
    }
}