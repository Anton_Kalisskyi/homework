import {Visit} from "./Visit.js";

export class VisitDentist extends Visit {
    constructor(doctor, name, title, description, urgency, lastDateVisit, status, id) {
        super(doctor, name, title, description, urgency, status, id);
        this.lastDateVisit = lastDateVisit
        this.lastDateVisitField = document.createElement('p')
    }
    renderVisitDentist(container) {
        super.renderVisit();
        container.append(this.visitWrapper)
    }
    showMore(container) {
        super.showMore()
        this.lastDateVisitField.innerHTML = `<span>Last visit: </span> ${this.lastDateVisit}`
        this.visitFieldsWrapper.append(this.lastDateVisitField)
    }
    showLess() {
        super.showLess();
        this.lastDateVisitField.remove()
    }
}