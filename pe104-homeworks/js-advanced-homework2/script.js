const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const rootElement = document.getElementById('root')
const elementsList = document.createElement('ul')

books.forEach((book) => {
    try {
        if (!book.author) {
            throw new Error('Book author had not been found')
        } else if (!book.name) {
            throw new Error('Book name had not been found')
        } else if (!book.price) {
            throw new Error('Book price had not been found')
        } else if (book.author && book.name && book.price) {
            elementsList.insertAdjacentHTML("afterbegin", `<li>author: ${book.author}, name: ${book.name}, price: ${book.price}</li>`)
        }
    }
    catch (e) {
        console.error(e)
    }
})
rootElement.append(elementsList)