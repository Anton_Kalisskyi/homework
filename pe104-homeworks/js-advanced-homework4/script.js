const url = 'https://ajax.test-danit.com/api/swapi/films'
const filmsWrapper = document.createElement('ul')

fetch(url)
    .then(response => response.json())
    .then((films) => {
        films.forEach((episode) => {
            const episodeItem = document.createElement('li')
            episodeItem.innerText = `Episode ${episode.episodeId}, ${episode.name}`
            const episodeDescription = document.createElement('p')
            filmsWrapper.insertAdjacentHTML('beforeend', '<br>')
            filmsWrapper.append(episodeItem)
            episodeDescription.innerText = `${episode.openingCrawl}`
            filmsWrapper.append(episodeDescription)
            const charactersWrapper = document.createElement('ul')
            filmsWrapper.append(charactersWrapper)
            episode.characters.forEach((link) => {
                fetch(`${link}`)
                    .then(response => response.json())
                    .then(data => {
                        charactersWrapper.insertAdjacentHTML('beforeend', `<li>${data.name}</li>`)
                    })
            })
        })
    })
document.body.append(filmsWrapper)