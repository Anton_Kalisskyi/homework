// Написати програму "Я тебе знайду по IP"
//
// Технічні вимоги:
//     Створити просту HTML-сторінку з кнопкою Знайти по IP.
//     Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json,
//     отримати звідти IP адресу клієнта.

//     Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/
//     та отримати інформацію про фізичну адресу.

//     під кнопкою вивести на сторінку інформацію, отриману з останнього запиту –
//     континент, країна, регіон, місто, район.

//     Усі запити на сервер необхідно виконати за допомогою async await.

// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

const API = 'https://api.ipify.org/?format=json'
const IP = 'http://ip-api.com/json/'
const button = document.querySelector('button')
const infoWrapper = document.createElement('div')
document.body.append(infoWrapper)

async function sendRequest(url, method = 'GET', ...options) {
    const response = await fetch(url, {
        method: method,
        ...options,
    })
    if (response.ok) {
        const result = response.json()
        return result
    } else {
        return new Error('Error')
    }
}
async function getIp(url) {
    const data = await sendRequest(url)
    const userIp = data.ip
    async function getInfo(url) {
        const info = await sendRequest(`${url}${userIp}`)
        console.log(info);
        const { countryCode, country, city, regionName, region } = info
        infoWrapper.innerText = '';
        infoWrapper.insertAdjacentHTML('beforeend', `
        <p class="info">Country Code: ${countryCode}</p>
        <p class="info">Country: ${country}</p>
        <p class="info">Region: ${regionName}</p>
        <p class="info">City: ${city}</p>
        <p class="info">District: ${region}</p>
    `)
    }
    await getInfo(IP)
}

button.addEventListener('click', async () => {
   await getIp(API)
})
