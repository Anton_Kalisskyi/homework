import React from "react";
import "./App.scss";
import Modal from "./components/Modal"
import Button from "./components/Button";

class App extends React.Component{
    state = {
        isModalOne: false,
        isModalTwo: false,
    };
    handleModal = (isModal) => {
        this.setState(prevState => ({
            [isModal]: !prevState[isModal],
        }))
    }

    handleOutside = (event, isModal) => {
        if(!event.target.closest(".modal")) {
            this.handleModal(isModal);
        }
    }
    render(){
        const {isModalOne, isModalTwo} = this.state
        return(
            <>
            <div className="buttons-wrapper">
                <Button
                    className = "button"
                    onClick = {() => this.handleModal("isModalOne")}
                    type = "button"
                    text = "Open first Modal"
                    backgroundColor = "rgb(103, 199, 241, 0.6)"
                />
                <Button
                    className = "button"
                    onClick = {() => this.handleModal("isModalTwo")}
                    type = "button"
                    text = "Open second Modal"
                    backgroundColor = "rgba(248, 255, 180, 0.6)"
                />
            </div>
                {isModalOne && <Modal
                    isModal = "isModalOne"
                    header = "Do you realy want to delete this file?"
                    text = "Once you delete this file, it won't be possible to undo this action."
                    handleOutside = {this.handleOutside}
                    closeModal = {() => this.handleModal("isModalOne")}
                    actions = {
                        <div className="button-wrapper">
                            <Button
                                className = "btn"
                                onClick = {() => this.handleModal("isModalOne")}
                                type = "button"
                                text = "Ok"
                            />
                            <Button
                                className = "btn"
                                onClick = {() => this.handleModal("isModalOne")}
                                type = "button"
                                text = "Cancel"
                            />
                        </div>
                    }
                />
                }
                {isModalTwo && <Modal
                    isModal = "isModalTwo"
                    header = "Do you realy want to delete this file?"
                    text = "Are you shure?"
                    handleOutside = {this.handleOutside}
                    closeModal = {() => this.handleModal("isModalTwo")}
                    actions = {
                        <div className="button-wrapper">
                            <Button
                                className = "btn"
                                onClick = {() => this.handleModal("isModalTwo")}
                                type = "button"
                                text = "Yes"
                            />
                            <Button
                                className = "btn"
                                onClick = {() => this.handleModal("isModalTwo")}
                                type = "button"
                                text = "No"
                            />
                        </div>
                    }
                />
                }
            </>
        )
    }
}

export default App;
