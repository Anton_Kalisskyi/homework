import React from "react";
import "./Button.scss"


class Button extends React.Component{
    render() {
        const {className, onClick, type, text, backgroundColor} = this.props
        return(
            <button
                className={className}
                onClick={onClick}
                type={type}
                style={{backgroundColor}}
                >
                {text}
            </button>
        )

    }
}

export default Button;