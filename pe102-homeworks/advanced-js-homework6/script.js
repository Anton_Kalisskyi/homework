// Завдання
// Написати програму "Я тебе знайду по IP"
//
// Технічні вимоги:
//     Створити просту HTML-сторінку з кнопкою Знайти по IP.
//     Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати
//     звідти IP адресу клієнта.

//     Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
//     під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
//     Усі запити на сервер необхідно виконати за допомогою async await.
//
// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

const API = `https://api.ipify.org/?format=json`
const ipAPI = 'http://ip-api.com/json/'
const findBtn = document.querySelector(".findIp") //ip-btn
const Info = document.querySelector(".ip-info") // add result from request


async function sendRequest(url, method = 'GET', options) {
    const response = await fetch(url, {
        method: method,
        ...options,
    })
    if (response.ok) {
        const result = response.json()
        return result
    }
    else {
        return new Error('Error')
    }
}

async function findIP () {
    const response = await sendRequest(API)
    console.log(response);
    const ipAdress = response.ip
}
async function findUser () {
    const ipInfo = await sendRequest (ipAPI)
    console.log(ipInfo);
    const {   countryCode, country, city, regionName, region} = ipInfo

    Info.insertAdjacentHTML(
        'beforeend',
        ` <p class="info">Country Code: ${countryCode}</p>
        <p class="info">Country: ${country}</p>
        <p class="info">Region: ${regionName}</p>
        <p class="info">City: ${city}</p>
        <p class="info">District: ${region}</p>
        
        `)

}
findBtn.addEventListener("click", async () => {
    findIP ()
    findUser ()

})



