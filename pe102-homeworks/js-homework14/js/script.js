const buttons = document.querySelectorAll('.btn-wrapper button');
document.addEventListener('keydown', function(event) {
    let pressedKey = event.key;
    buttons.forEach(button => {
        button.classList.remove('active')
        if (button.dataset.key === pressedKey) {
            button.classList.add('active')
        }
    })
})

// Реалізувати можливість зміни колірної теми користувача.

let themeButton = document.querySelector('.theme-btn');
themeButton.addEventListener('click', (event)=> {
    event.preventDefault();
    if (localStorage.getItem('theme') === 'dark') {
        localStorage.removeItem('theme')
    } else {
        localStorage.setItem('theme', 'dark')
    }
    themeSwitcher();
})
function themeSwitcher() {
    if (localStorage.getItem('theme') === 'dark') {
        document.querySelector('html').classList.add('dark')
        document.querySelector('.material-symbols-rounded').innerText = 'dark_mode'
        document.querySelector('.material-symbols-rounded').style.color = 'white'
    } else {
        document.querySelector('html').classList.remove('dark')
        document.querySelector('.material-symbols-rounded').innerText = 'light_mode'
        document.querySelector('.material-symbols-rounded').style.color = 'black'
    }
}
themeSwitcher();