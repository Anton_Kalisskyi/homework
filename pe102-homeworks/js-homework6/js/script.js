function createNewUser() {
    let firstName = prompt("Please, enter your name");
    let lastName = prompt("Please, enter your surname");
    let birthday = prompt("Enter your Date of Birth in the dd.mm.yyyy format")
    let currentDate = new Date();
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        },
        getAge() {
            birthday = birthday.split('.');
            birthday = new Date(birthday[2], birthday[1] - 1, birthday[0]);
            if (currentDate.getMonth() >= birthday.getMonth()) {
                if (currentDate.getDate() >= birthday.getDate()) {
                    return currentDate.getFullYear() - birthday.getFullYear()
                } return currentDate.getFullYear() - birthday.getFullYear() - 1
            } return currentDate.getFullYear() - birthday.getFullYear() - 1
        },
        // getAge() {
        //     if ((Number(currentDate.getMonth()) +1) >= Number(birthday.slice(3, 5))) {
        //         if ((Number(currentDate.getDate())) >= Number(birthday.slice(0, 2))) {
        //             return Number(currentDate.getFullYear() - Number(birthday.slice(-4)));
        //         } return Number(currentDate.getFullYear() - Number(birthday.slice(-4)) - 1)
        //     } return Number(currentDate.getFullYear() - Number(birthday.slice(-4)) - 1)
        // },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
        }
    }
    return newUser
}
let user = createNewUser(); // Створено юзера за допомогою функції createNewUser()

console.log(user.getLogin()); // Запустили метод getLogin() і вивели в консоль його значення

console.log(user); // Вивели у консоль об'єкт user як результат роботи функції createNewUser()
console.log(user.getAge()); // Запустили метод getAge() і вивели в консоль його значення
console.log(user.getPassword()); // Запустили метод getPassword() і вивели в консоль його значення

