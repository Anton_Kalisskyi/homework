let number1 = +prompt("Enter your first number");
while (number1 === null ||  Number.isNaN(number1)) {
    number1 = +prompt("Enter the first number", number1);
}
let number2 = +prompt("Enter your second number");
while (number2 === null || Number.isNaN(number2)) {
    number2 = +prompt("Enter the second number", number2);
}
let mathOperator = prompt(`Choose the math. operation: "+", "-", "*", "/"`);
while (!(mathOperator === "+" || mathOperator === "-" || mathOperator === "*" || mathOperator === "/")) {
    mathOperator = prompt(`Choose the math. operation: "+", "-", "*", "/"`)
}

function mathOperation (a, b, operator) {
    switch (operator) {
        case "+" :
            return a + b;
            break;
        case "-" :
            return a - b;
            break;
        case "*" :
            return a * b;
            break;
        case "/" :
            return a / b;
            break;
    }
}

// function mathOperation (a, b, operator) {
//     if (operator === "+") {
//         return a + b;
//     } else if (operator === "-") {
//         return a - b;
//     } else if (operator === "*") {
//         return a * b;
//     } else if (operator === "/") {
//         return a / b;
//     }
// }

console.log(mathOperation(number1, number2, mathOperator))






