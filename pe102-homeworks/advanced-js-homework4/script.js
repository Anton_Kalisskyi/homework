
const API = 'https://ajax.test-danit.com/api/swapi/films';
const filmsWrapper = document.getElementById('films-wrapper');
const filmsList = document.createElement('ul');

filmsWrapper.append(filmsList);

function getRespond(url) {
    const sendRequest = fetch(url)
        .then((response) => response.json())
        .then((data) => {
            data.forEach(element => {
                const episode = document.createElement('li')
                episode.innerText = `Episode ${element.episodeId}: "${element.name}"`
                episode.insertAdjacentHTML("beforeend", `<p>${element.openingCrawl}</p>`)
                filmsList.append(episode)
                const charactersList = document.createElement('ul');
                episode.append(charactersList);
                element.characters.forEach(e => {
                    const sendRequest = fetch(e)
                        .then((response) => response.json())
                        .then((data) => {
                            charactersList.insertAdjacentHTML("beforeend", `<li>${data.name}</li>`)
                        })
                    })
                episode.insertAdjacentHTML("beforeend", '<br>')
            })
        })
}

getRespond(API)