const allInputIcons = document.querySelectorAll('.fas');
allInputIcons.forEach(icon => {
    icon.addEventListener('click', () => {
        if (icon.closest('.input-wrapper').querySelector('input').type === 'password') {
            icon.closest('.input-wrapper').querySelector('input').type = 'text';
        } else {
            icon.closest('.input-wrapper').querySelector('input').type = 'password';
        }
    const closestInputIcons = icon.closest('.input-wrapper').querySelectorAll('i');
        closestInputIcons.forEach(icon => {
         if (icon.classList.contains('hidden')) {
             icon.classList.remove('hidden')
         }
     })
        icon.classList.add('hidden');
    })
})
const inputButton = document.querySelector('.btn');
const lastInput = document.getElementById('check-password')
const warningText = document.createElement('span');
warningText.innerText = 'Потрібно ввести однакові значення';
warningText.classList.add('warning-text');

inputButton.addEventListener('click', (event) => {
    event.preventDefault();
    let passwordValue = document.querySelector('#enter-password').value;
    let checkPasswordValue = document.querySelector('#check-password').value;
    if (!(passwordValue) || !(checkPasswordValue) || !(passwordValue === checkPasswordValue)) {
        lastInput.insertAdjacentElement("afterend", warningText);
    } else {
        setTimeout('alert("You are welcome")',0);
        document.querySelector('.warning-text').remove();
    }
})

