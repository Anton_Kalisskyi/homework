class Employee {
    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }
    get name(){
        return this._name.toLowerCase()
    }
    set name(name){
        return this._name = name
    }
    get age(){
        return this._age
    }
    set age(age){
        return this._age
    }
    get salary(){
        return this._salary
    }
    set salary(salary){
        return this._salary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang
    }
    get salary(){
        return this._salary * 3
    }
}

console.log(new Programmer('Maria', 25, 7000, 'uk'));
console.log(new Programmer('Mike', 30, 4000, 'eng'));
console.log(new Programmer('Me', 39, 5000, 'uk'));
// const pr1 = new Programmer('Maria', 25, 7000, 'uk')
// console.log(pr1.salary)