
const listCreation = function (array, parentElement = document.body) {
    const list = document.createElement('ul');
    array.map(item => {
        if (Array.isArray(item)) {
            listCreation(item, list)
        } else {
            let listElement = document.createElement('li');
            listElement.innerText = item;
            list.append(listElement);
        }
    });
    parentElement.append(list);
}
listCreation(["Kharkiv", "Kyiv", ["Borispol", "Irpin", ["Bucha", "Gostomel"]], "Odesa", ["Zatoka", "Bilgorod-Dnistrovskyi"], "Lviv", "Dnipro"]);

//     Додаткове завдання:
//     Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

const timerCreation = function (counter) {
    const timer = document.createElement('span');
    timer.style.backgroundColor = 'red';
    timer.style.padding = '10px';
    timer.innerHTML = `<strong>${counter}</strong>`;
    document.body.append(timer);
    function setTimerInterval() {
        if (counter > 0) {
            counter--
            timer.innerHTML = `<strong>${counter}</strong>`;
        } else {
            deleteContent()
        }
    }
    setInterval(setTimerInterval,1000);
}

timerCreation(3)

function deleteContent() {
    document.body.innerHTML = '';
}
