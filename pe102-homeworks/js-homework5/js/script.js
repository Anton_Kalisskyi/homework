function createNewUser() {
    let firstName = prompt("Please, enter your name");
    let lastName = prompt("Please, enter your surname");
    const newUser = {
        _firstName: firstName,
        _lastName: lastName,
        getLogin() {
            return (this._firstName[0] + this._lastName).toLowerCase()
        },
        set firstName(newName) {
            this._firstName = newName
        },
        set lastName(newSurname) {
            this._lastName = newSurname
        },
    }
    return newUser
}
let user = createNewUser(); // Створено юзера за допомогою функції createNewUser()

console.log(user.getLogin()); // Запустили метод getLogin() і вивели в консоль його значення

user.firstName = "Good" // Змінюємо значення user.firstName
user.lastName = "Boy" // Змінюємо значення user.lastName

console.log(user.getLogin()); // Знову запустили метод getLogin() і вивели в консоль його нове значення
console.log(user); // Вивели в консоль об'єкт
