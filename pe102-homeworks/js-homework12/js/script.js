const buttons = document.querySelectorAll('button');
document.addEventListener('keydown', function(event) {
    let pressedKey = event.key;
    buttons.forEach(button => {
        button.classList.remove('active')
        if (button.dataset.key === pressedKey) {
            button.classList.add('active')
        }
    })
})
