//     Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let paragraphs = Array.from(document.querySelectorAll('p'))
const changeColor = function(elements) {
    for (let element of elements) {
        element.style.background = '#ff0000';
    }
};
changeColor(paragraphs);
// //     Знайти елемент із id="optionsList". Знайти батьківський елемент та вивести в консоль.
let element = document.getElementById('optionsList')
// //     Вивести у консоль.
console.log(element);
// //     Знайти батьківський елемент та вивести в консоль.
console.log(element.parentElement);
// //     Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
console.log(element.childNodes);
// //     Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
document.querySelector('#testParagraph').innerText = 'This is a paragraph';
// //
// // //     Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
let elements = document.querySelector('.main-header').children;
console.log(elements);
// //     Кожному з елементів присвоїти новий клас nav-item.
Array.from(elements).forEach((element) => {
    element.classList.add('nav-item')
});
// // //     Знайти всі елементи із класом section-title.
let sectionTitleElements = Array.from(document.querySelectorAll('.section-title'));
// // //     Видалити цей клас у цих елементів.
const deleteClass = function (elements) {
    for (let element of elements) {
        element.classList.remove('section-title')
    }
}
deleteClass(sectionTitleElements);





