const tabs = document.querySelectorAll('.tabs-title');
const articles = document.querySelectorAll('.tabs-content li');

const switchingTabs = function (tabs, articles) {
        tabs.forEach(tab => {
            let tabDataAttribute = tab.innerText.toLowerCase();
            tab.dataset.id = tabDataAttribute;
            tab.addEventListener('click', () => {
                tabs.forEach(tab => {
                    tab.classList.remove('active');
                })
                articles.forEach(article => {
                    if (article.dataset.content.includes(tabDataAttribute)) {
                        article.className = 'article';
                        tab.classList.add('active');
                    } else {
                        article.className = 'hidden-content';
                    }
                })
            });
        });
};
switchingTabs(tabs, articles);

// let ul = document.querySelector('.tabs-title');
// ul.addEventListener('click', function (event) {
// console.log(event.target.dataset.type);
// let data = event.target.dataset.type;
// console.log(document.querySelector('.active-p'));
// // document.querySelector('.active-p').classList.remove('active-p');
// // console.log(document.querySelector('.active-tab'));
// // document.querySelector('.active-tab').classList.remove('active-tab');
// // console.log(document.querySelector(`[data-li = ${data}]`));
// // document.querySelector(`[data-li = ${data}]`).classList.add('active-p');
// // ev.target.classList.add('active-tab') });

