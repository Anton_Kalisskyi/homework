// Теоретичні питання
// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

const images = document.querySelectorAll('.image-to-show');
const firstImage = images[0];
const lastImage = images[images.length - 1];
const buttonStart = document.querySelector('.start');
const buttonStop = document.querySelector('.stop');

const slider = function() {
    const currentImage = document.querySelector('.visible');
    if (currentImage !== lastImage) {
        currentImage.classList.remove('visible');
        currentImage.nextElementSibling.classList.add('visible');
    } else {
        currentImage.classList.remove('visible');
        firstImage.classList.add('visible');
    }
};
let timer = setInterval(slider, 3000);
buttonStart.addEventListener('click', () => {
    timer = setInterval(slider, 3000);
    buttonStart.disabled = true;
    buttonStop.disabled = false;
});
buttonStop.addEventListener('click', () => {
    clearInterval(timer);
    buttonStart.disabled = false;
    buttonStop.disabled = true;
});

