// Технічні вимоги:
//     При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій.
//     Для цього потрібно надіслати GET запит на наступні дві адреси:
//     https://ajax.test-danit.com/api/json/users
//     https://ajax.test-danit.com/api/json/posts
//             Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
//     Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок,
//     текст, а також ім'я, прізвище та імейл користувача, який її розмістив.

//     На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки.
//     При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
//     Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки,
//     використовуючи JavaScript.

//     Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
//     Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені.
//     Це нормально, все так і має працювати.

//     Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card.
//     При необхідності ви можете додавати також інші класи.

// Необов'язкове завдання підвищеної складності
//     Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження.
//     Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

//     Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно,
//     в якому користувач зможе ввести заголовок та текст публікації.
//     Після створення публікації дані про неї необхідно надіслати в POST запиті
//     на адресу: https://ajax.test-danit.com/api/json/posts.
//     Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку).
//     Автором можна присвоїти публікації користувача з id: 1.

//     Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно
//     надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.

const postsWrapper = document.getElementById('posts')
const API = 'https://ajax.test-danit.com/api/json/'


// включати заголовок,
//     текст, а також ім'я, прізвище та імейл користувача, який її розмістив.

class userCard {
    constructor({name, username, email, title, body, id}) {
        this.name = name
        this.username = username
        this.email = email
        this.title = title
        this.body = body
        this.id = id
    }
    render(element){
        element.insertAdjacentHTML("beforeend", `
        <div class="user-cards" id=${this.id}>
        <p>${this.name}</p>
        <p>${this.username}</p>
        <p>${this.email}</p>
        <p>${this.title}</p>
        <p>${this.body}</p>
        <button>delete</button>
    </div>
        `)
        const post = document.getElementById(`${this.id}`)
        const deleteBtn = post.querySelector('button').onclick = this.delete
    }
    delete = async () => {
        const post = document.getElementById(`${this.id}`)
        const deleteAPI = `https://ajax.test-danit.com/api/json/posts/${this.id}`
        const deletePost = await sendRequest(deleteAPI, 'DELETE')
        if (deletePost.ok) {
            post.remove()
        }
    }
}

async function sendRequest(url, method = "GET", options) {
    const response = await fetch(url, {
        method: method,
        ...options,
    })
    if (response.ok) {
        if (method !== "DELETE") {
            const result = response.json()
            return result
        } else {
            return response
        }
    } else {
        return new Error("Error")
    }

}

async function getUser() {
    const newUser = await sendRequest(`${API}users`)
    // console.log(newUser);
    newUser.forEach((element) => {
        console.log(element);

        async function getPosts() {
            const post = await sendRequest(`${API}users/${element.id}/posts`)
            // console.log(post);
            post.forEach(e => {
                const user = new userCard({...element, ...e})
                user.render(postsWrapper)
            })
        }
        getPosts()
    })
}
getUser()
