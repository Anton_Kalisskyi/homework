document.addEventListener('DOMContentLoaded', () => {
    const tabsWrapper = document.querySelector('.services-list');
    tabsWrapper.addEventListener('click', function (event) {
        let data = event.target.dataset.id;
        document.querySelector('.active-tab').classList.remove('active-tab');
        document.querySelector('.service-article').className = 'hidden-content';
        document.querySelector(`[data-content = ${data}]`).className = 'service-article';
        event.target.classList.add('active-tab')
    });

    const works = document.querySelector('.works-list');
    let designs = document.querySelectorAll('.image-wrapper');
    const loadMoreBtn = document.getElementById('load-more-button');
    let numberOfVisibleDesigns = 12;

    works.addEventListener('click', function (event) {
        let data = event.target.dataset.type;
        document.querySelector('.active').classList.remove('active');
        event.target.classList.add('active');
        designs.forEach(design => {
            design.classList.add('hidden-image')
            if (data === design.dataset.design) {
                design.classList.remove('hidden-image')
                loadMoreBtn.style.display = 'none';
            } else if (data === 'all') {
                    for (let i = 0; i < 12; i++) {
                        designs[i].className = 'image-wrapper';
                    }
                loadMoreBtn.style.display = 'flex';
                numberOfVisibleDesigns = 12;
            }
        })
    })

    loadMoreBtn.addEventListener('click', function () {
        for (let i = numberOfVisibleDesigns; i < numberOfVisibleDesigns + 12; i++) {
            designs[i].className = 'image-wrapper';
        }
        numberOfVisibleDesigns += 12;
        if (numberOfVisibleDesigns >= designs.length) {
            loadMoreBtn.style.display = 'none';
        }
    })

    let swiper = new Swiper(".mySwiper", {
        slidesPerView: 'auto',
        freeMode: true,
        watchSlidesProgress: true,
    });
    let swiper2 = new Swiper(".mySwiper2", {
        loop: true,
        initialSlide: 1,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        thumbs: {
            swiper: swiper,
        },
    });
})


