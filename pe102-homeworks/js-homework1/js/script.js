    //     Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.

    let admin, name;
    name = "Anton";
    admin = name;
    console.log(admin);

    // Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.

    const days = 10;
    let seconds = days * 24 * 60 * 60;
    console.log(`${days} days is ${seconds} seconds`);

    //     Запитайте у користувача якесь значення і виведіть його в консоль.

    let userNumber = parseInt(prompt ("Enter the number"));
    console.log(userNumber);