const array = ["1", "2", "3", "sea", "user", 23];
const domElement = document.querySelector('div');

const getList = (array, domElement = document.body) => {
    domElement.insertAdjacentHTML('beforeend', `<ul></ul>`);
    const list = document.querySelector('ul');
    array.map(item => {
        list.insertAdjacentHTML('beforeend', `<li>${item}</li>`);
    });
}
getList (array, domElement);
