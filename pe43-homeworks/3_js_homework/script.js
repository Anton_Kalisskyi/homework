let number1 = +prompt('Введите первое число');
while (!Number.isInteger(number1)) {
    number1 = +prompt('Введите именно число', number1);
}

let number2 = +prompt('Введите второе чилсо');
while (!Number.isInteger(number2)) {
    number2 = +prompt('Введите именно число', number2);
}

let mathAction = prompt ('Выберете математическую операцию: "+", "-", "*", "/".')
while (!(mathAction === '+' || mathAction === '-' || mathAction === '*' || mathAction === '/')) {
    mathAction = prompt ('Выберете математическую операцию: "+", "-", "*", "/".')
}

function calculate(number1, number2, mathAction) {
    switch (mathAction) {
        case '+' :
            return number1 + number2;
        case '-' :
            return number1 - number2;
        case '*' :
            return number1 * number2;
        case '/' :
            return number1 / number2;
    }
}
alert(calculate(number1, number2, mathAction));
