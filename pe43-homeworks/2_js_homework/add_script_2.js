let m = +prompt('Введите меньшее число');
let n = +prompt('Введите большее число');
while (isNaN(n) || isNaN(m) || m > n) {
    m = +prompt('Введите меньшее число');
    n = +prompt('Введите большее число');
}
for (let i = m; i <= n; i++) {

    for (let j = 2; j < i; j++) {
        if (i % j === 0) {
            break;
        } else if (j === i-1) {
            console.log(`Число ${i} простое`);
        }
    }
}