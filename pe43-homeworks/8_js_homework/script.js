const input = document.querySelector('input');
let span = document.createElement('span');
let button = document.createElement('button');
const warning = document.createElement('p')

input.addEventListener('focus',() => {
    input.style.borderColor = 'green';
    input.style.outline = 'none';
    warning.remove();
});
input.addEventListener('focusout',() => {
    let inputValue = input.value;
    if (inputValue < 0) {
        input.style.borderColor = 'red';
        warning.innerHTML = `Please enter correct price`;
        input.after(warning);
        warning.style.color = 'red';
    }
    else {
        span.innerHTML = `Текущая цена: ${inputValue}`;
        button.innerHTML = 'x';
        document.body.prepend(span);
        span.after(button);
        input.style.color = 'green';
        input.style.borderColor = '';
        button.addEventListener('click', () => {
            span.remove();
            button.remove();
            input.value = '';
        });
    }
});