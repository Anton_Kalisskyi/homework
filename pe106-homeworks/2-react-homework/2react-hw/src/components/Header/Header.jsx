import React from 'react';

import "./Header.scss"
import CartIcon from "../ui/icons/CartIcon.jsx"
import FavoritesIcon from "../ui/icons/FavoritesIcon.jsx";
import PropTypes from "prop-types";

const Header = ({favoriteProductsCounter, cartProductsCounter}) => {
    return (
        <header className='header'>
            <div className="header-icons-wrapper">
                <a href="#">
                    <div className="header-icon">
                        <FavoritesIcon/>
                        <span className="header-icon-counter">{favoriteProductsCounter}</span>
                    </div>
                </a>
                <a href="#">
                    <div className="header-icon">
                        <CartIcon/>
                        <span className="header-icon-counter">{cartProductsCounter}</span>
                    </div>
                </a>
            </div>
        </header>
    );
};

Header.propTypes = {
    favoriteProductsCounter: PropTypes.number,
    cartProductsCounter: PropTypes.number,

}

export default Header;