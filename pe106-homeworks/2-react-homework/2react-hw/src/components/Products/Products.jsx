import React, {useState, useEffect} from 'react';

import Product from "./components/Product/Product.jsx"
import ProductModalAddToCart from "./components/ProductModals/ProductModalAddToCart.jsx";
import {sendRequest} from "../../helpers/sendRequest.js";

import "./Products.scss"
import PropTypes from "prop-types";
import Button from "../Button/Button.jsx";

const Products = ({onFavorite, onCart}) => {

    const [products, setProducts] = useState([])
    const [currentProduct, setCurrentProduct] = useState({})
    const [isModal, setIsModal] = useState(false)

    const handleCurrentProduct = (product) => {
        return setCurrentProduct(product)
    }

    const handleModal = () => setIsModal(!isModal)

    useEffect(() => {
        sendRequest('/products.json')
            .then((data) => setProducts(data))
    }, [])

    console.log(products);

    return (
        <>
            <main className="main-content">
                <div className="products-wrapper">
                    {products?.map((product) => <Product
                        key={product.id}
                        id={product.id}
                        name={product.name}
                        price={product.price}
                        picture={product.picture}
                        color={product.color}
                        onClick={()=> {
                            handleCurrentProduct(product)
                            handleModal()
                            }
                        }
                        onFavorite={() => {
                            onFavorite(product)
                            }
                        }
                    />)
                    }
                </div>
            </main>
                <ProductModalAddToCart
                    onClose={handleModal}
                    isOpen={isModal}
                    name={currentProduct.name}
                    picture={currentProduct.picture}
                    onCart={() => {
                        onCart(currentProduct)
                        }
                    }
                />
        </>
    );
};

Products.propTypes = {
    onCart: PropTypes.func,
    onFavorite: PropTypes.func,

}

export default Products;