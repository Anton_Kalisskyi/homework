import React from 'react';
import PropTypes from "prop-types";

import ModalWrapper from "./ModalWrapper.jsx";
import ModalBox from "./ModalBox.jsx";
import ModalClose from "./ModalClose.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalFooter from "./ModalFooter.jsx";

const ModalText = ({close, isOpen}) => {

    return (
        <ModalWrapper close={close} isOpen={isOpen}>
            <ModalBox>
                <ModalClose close={close}/>
                <ModalHeader>
                    <h6>Add Product "Name"</h6>
                </ModalHeader>
                <ModalBody>
                    <p>Description for your product</p>
                </ModalBody>
                <ModalFooter
                    firstText = "Add to favorite"
                    firstClick = {()=>{}}
                >
                </ModalFooter>
            </ModalBox>
        </ModalWrapper>
    );
};

ModalText.propTypes = {
    close: PropTypes.func,
    isOpen: PropTypes.bool,
}

export default ModalText;