import React from 'react';

import Button from "../Button/Button.jsx";
import PropTypes from "prop-types";

const ModalFooter = ({onClose, onCart}) => {

        return (
            <div className="modal-footer">
                <div className="btn-wrapper">
                    <Button type="button"
                            onClick={() => {
                                onCart()
                                onClose()
                                }
                            }
                            className="modal-btn"
                            text="Confirm"
                    />
                    <Button type="button" onClick={onClose} className="modal-btn" text="Cancel"/>
                </div>
            </div>
        );
};

ModalFooter.propTypes = {
    onCart: PropTypes.func,
    onClose: PropTypes.func,
}

export default ModalFooter;