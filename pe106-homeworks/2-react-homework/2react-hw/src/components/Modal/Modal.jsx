import React from 'react';
import PropTypes from "prop-types";

import ModalWrapper from "./ModalWrapper.jsx";
import ModalBox from "./ModalBox.jsx";
import ModalClose from "./ModalClose.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalFooter from "./ModalFooter.jsx";

const Modal = ({onClose, children, isOpen, name, picture}) => {
    return (
            <ModalWrapper onClose={onClose} isOpen={isOpen}>
                <ModalBox>
                    <ModalClose onClose={onClose}/>
                    <ModalHeader>{children}</ModalHeader>
                    <ModalBody name={name} href={picture}>{children}</ModalBody>
                    <ModalFooter onClose={onClose} isOpen={isOpen}>{children}</ModalFooter>
                </ModalBox>
            </ModalWrapper>
    );
};

Modal.propTypes = {
    close: PropTypes.func,
    isOpen: PropTypes.bool,
    children: PropTypes.any,
}

export default Modal;