import React from 'react';
import Close from "./icons/close.svg?react";
import PropTypes from "prop-types";

const ModalClose = ({onClose}) => {

    return (
        <button type="button" onClick={onClose} className="modal-close">
            <Close/>
        </button>
    );
};

ModalClose.propTypes = {
    close: PropTypes.func,
}

export default ModalClose;