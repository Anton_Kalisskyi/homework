import React from "react";
import PropTypes from "prop-types";
import cn from "classNames"

import "./Button.scss";

const Button = (props) => {

    const {
        type,
        className,
        onClick,
        children,
        ...restProps
    } = props

    return(
        <button type={type} className={cn("button", className)} onClick={onClick} {...restProps} >{children}</button>
    )
}

Button.defaultProps = {
    type: "button"
}

Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.any,
    restProps: PropTypes.object,

}

export default Button