import React from 'react';

import "./Modal.scss"
import PropTypes from "prop-types";

const ModalWrapper = ({children, close}) => {
    return (
        <div className="modal-wrapper" onClick={(e) => {
            if (e.target.classList.contains("modal-wrapper")) {
                close();
            }
        }}>
            {children}
        </div>
    );
};

ModalWrapper.propTypes = {
    close: PropTypes.func,
    children: PropTypes.any,
}

export default ModalWrapper;