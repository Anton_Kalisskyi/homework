import React from 'react';
import ModalWrapper from "./ModalWrapper.jsx";
import ModalBox from "./ModalBox.jsx";
import ModalClose from "./ModalClose.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalFooter from "./ModalFooter.jsx";
import PropTypes from "prop-types";

const ModalText = ({close}) => {

    return (
        <ModalWrapper close={close}>
            <ModalBox>
                <ModalClose close={close}/>
                <ModalHeader>
                    <h6>Add Product "Name"</h6>
                </ModalHeader>
                <ModalBody>
                    <p>Description for your product</p>
                </ModalBody>
                <ModalFooter
                    firstText = "Add to favorite"
                    firstClick = {()=>{}}
                >
                </ModalFooter>
            </ModalBox>
        </ModalWrapper>
    );
};

ModalText.propTypes = {
    close: PropTypes.func,
}

export default ModalText;