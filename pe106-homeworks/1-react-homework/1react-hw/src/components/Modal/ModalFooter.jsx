import React from 'react';

import Button from "../Button/Button.jsx";
import PropTypes from "prop-types";

const ModalFooter = (props) => {

    const {
        firstText,
        secondaryText,
        firstClick,
        secondaryClick
    } = props

    if (firstText && secondaryText) {
        return (
            <div className="modal-footer">
                <div className="btn-wrapper">
                    <Button type="button" onClick={firstClick} className="modal-btn">{firstText}</Button>
                    <Button type="button" onClick={secondaryClick} className="modal-btn">{secondaryText}</Button>
                </div>
            </div>
        );
    }
    return (
        <div className="modal-footer">
            <div className="btn-wrapper">
                <Button type="button" onClick={firstClick} className="modal-btn">{firstText}</Button>
            </div>
        </div>
    );
};

ModalFooter.propTypes = {
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func,
}

export default ModalFooter;