import React from 'react';

import ModalWrapper from "./ModalWrapper.jsx";
import ModalBox from "./ModalBox.jsx";
import ModalClose from "./ModalClose.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalFooter from "./ModalFooter.jsx";
import PropTypes from "prop-types";
import Button from "../Button/Button.jsx";

const Modal = ({close, children}) => {
    return (
            <ModalWrapper close={close}>
                <ModalBox>
                    <ModalClose close={close}/>
                    <ModalHeader>{children}</ModalHeader>
                    <ModalBody>{children}</ModalBody>
                    <ModalFooter >{children}</ModalFooter>
                </ModalBox>
            </ModalWrapper>
    );
};

Modal.propTypes = {
    close: PropTypes.func,
    children: PropTypes.any,
}

export default Modal;