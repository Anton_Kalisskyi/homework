import React, {useState} from 'react'

import Button from "./components/Button/Button.jsx";
import Modal from "./components/Modal/Modal.jsx";
import ModalText from "./components/Modal/ModalText.jsx";
import ModalImage from "./components/Modal/ModalImage.jsx";

import './App.scss'

function App() {
    const [isModal, setIsModal] = useState(false)
    const [isModalText, setIsModalText] = useState(false)
    const [isModalImage, setIsModalImage] = useState(false)

    const handleModal = () => {
        setIsModal((prevState) => !prevState)
    }

    const handleModalText = () => {
        setIsModalText((prevState) => !prevState)
    }
    const handleModalImage = () => {
        setIsModalImage((prevState) => !prevState)
    }

  return (
      <div className="btn-wrapper">
          <Button type="button" className="btn" onClick={handleModalImage}>Open first modal</Button>
          <Button type="button" className="btn" onClick={handleModalText}>Open second modal</Button>
          {/*{isModal && <Modal close={handleModal}/>}*/}
          {isModalImage && <ModalImage close={handleModalImage}/>}
          {isModalText && <ModalText close={handleModalText}/>}
      </div>
  )
}

export default App
