import React from 'react';
import Close from "./icons/close.svg?react";
import {useDispatch} from "react-redux"
import {toggleModalAction} from "../../store/slices/modalSlice.js";

const ModalClose = () => {

    const dispatch = useDispatch()

    return (
        <button type="button" className="modal-close">
            <Close onClick={()=>{dispatch(toggleModalAction())}}/>
        </button>
    );
};

export default ModalClose;