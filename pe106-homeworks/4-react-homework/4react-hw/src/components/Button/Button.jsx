import React from "react";
import PropTypes from "prop-types";
import cn from "classnames"

import "./Button.scss";

const Button = (props) => {

    const {
        type,
        className,
        onClick,
        children,
        text,
        ...restProps
    } = props

    return(
        <button type={type} className={cn("button", className)} onClick={onClick} {...restProps} >{text}</button>
    )
}

Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.any,
    restProps: PropTypes.object,

}

export default Button