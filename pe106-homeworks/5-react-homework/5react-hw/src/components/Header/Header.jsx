import React from 'react';
import CartIcon from "../ui/icons/CartIcon.jsx"
import FavoritesIcon from "../ui/icons/FavoritesIcon.jsx";
import MenuIcon from "../ui/icons/MenuIcon.jsx";
import {Link} from "react-router-dom";
import {useSelector} from 'react-redux';
import "./Header.scss"


const Header = () => {
    const favoriteProducts = useSelector((state) => state.favorites.favoriteProducts);
    const cartProducts = useSelector((state) => state.cart.cartProducts);

    return (
        <header className='header'>
            <Link to={"/"}>
            <div className='header-logo'>
                <h3 className="header-logo-text">Logo</h3>
                <div className='header-logo-icon'>
                    <MenuIcon />
                </div>
            </div>
            </Link>
            <div className="header-icons-wrapper">
                <Link to={"/wishlist"}>
                    <div className="header-icon">
                        <FavoritesIcon/>
                        <span className="header-icon-counter">{favoriteProducts?.length}</span>
                    </div>
                </Link>
                <Link to={"/cart"}>
                    <div className="header-icon">
                        <CartIcon/>
                        <span className="header-icon-counter">{cartProducts.length}</span>
                    </div>
                </Link>
            </div>
        </header>
    );
};

export default Header;