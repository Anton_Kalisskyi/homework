import React from 'react';
import cn from 'classnames'
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import Button from "../../../Button/Button.jsx";
import FavoritesIcon from "../../../ui/icons/FavoritesIcon.jsx";
import {toggleFavoritesAction} from "../../../../store/slices/favoritesSlice.js"
import {updateModalContentAction} from "../../../../store/slices/modalSlice.js"
import {toggleModalAction} from "../../../../store/slices/modalSlice.js"
import {useSelector, useDispatch} from "react-redux"

import "./Product.scss"

const Product = (props) => {

    const {
        id,
        name,
        price,
        picture,
        color,
        linkPath,
    } = props

    const dispatch = useDispatch()

    const allFavorites = useSelector((state) => state.favorites.favoriteProducts)
    const allCartProducts = useSelector((state) => state.cart.cartProducts)


    const isFavoriteTrue = allFavorites?.some((item) => item.id === id)
    const isAddedToCartTrue = allCartProducts.some((item) => item.id === id)

    const handleFavorite = (product) => {
        dispatch(toggleFavoritesAction(product))
    }

    const handleCart = () => {
        dispatch(updateModalContentAction({name, id, picture, price, color, linkPath}))
        dispatch(toggleModalAction())
    }

    return (
        <div className="product">
            <div className="product-icons-wrapper" onClick={()=> {
                handleFavorite({
                    id,
                    name,
                    price,
                    picture,
                    color,
                    linkPath,
                })
                }
            }
            >
                <FavoritesIcon className={cn("product-icon", {"is-favorite" : isFavoriteTrue})}/>
            </div>
            <Link to={linkPath} className="product-link">
            <div className="product-image">
                <img
                    src={picture}
                    alt={name}/>
            </div>
            </Link>
            <div className="product-details">
                <Link to={linkPath} className="product-link">
                <h3 className="product-name">{name}</h3>
                </Link>
                <span className="product-color">{`color: ${color}`}</span>
                <span className="product-ia">{`id: ${id}`}</span>
            </div>
            <div className="btn-wrapper">
                <Button
                    type="button"
                    className={isAddedToCartTrue ? "product-btn_painted" : "product-btn"}
                    onClick={handleCart}
                    text={isAddedToCartTrue ? "In the cart" : "Add to cart"}></Button>
                <p className="product-price">{`$${price}`}</p>
            </div>
        </div>
    );
};

Product.propTypes = {
    id: PropTypes.number,
    price: PropTypes.number,
    name: PropTypes.string,
    picture: PropTypes.string,
    color: PropTypes.string,
}

export default Product;