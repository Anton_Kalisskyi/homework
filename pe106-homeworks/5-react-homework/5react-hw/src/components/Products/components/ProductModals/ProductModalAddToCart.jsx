import React from "react";
import PropTypes from "prop-types";

import ModalWrapper from "../../../Modal/ModalWrapper.jsx";
import ModalBox from "../../../Modal/ModalBox.jsx";
import ModalClose from "../../../Modal/ModalClose.jsx";
import ModalHeader from "../../../Modal/ModalHeader.jsx";
import ModalBody from "../../../Modal/ModalBody.jsx";
import ModalFooter from "../../../Modal/ModalFooter.jsx";
import {useSelector} from "react-redux";

const ProductModalAddToCart = ({children}) => {

    const cartProducts = useSelector((state) => state.cart.cartProducts)
    const modalProduct = useSelector((state) => state.modal.modalContent)

    const isAddedToCartTrue = cartProducts.some((item) => item.id === modalProduct.id)

    return (
        <ModalWrapper>
            <ModalBox>
                <ModalClose/>
                <ModalHeader title={isAddedToCartTrue ? "Remove this product from the cart?" : "Add this product to the cart?"}/>
                <ModalBody>{children}</ModalBody>
                <ModalFooter>{children}</ModalFooter>
            </ModalBox>
        </ModalWrapper>
    );
};

ProductModalAddToCart.propTypes = {
    children: PropTypes.any,
}

export default ProductModalAddToCart;