import React, {useEffect} from 'react';
import {useSelector, useDispatch} from "react-redux"
import Product from "./components/Product/Product.jsx"
import {fetchProducts} from "../../store/slices/productsSlice.js"
import "./Products.scss"

const Products = () => {

    const dispatch = useDispatch();
    const products = useSelector((state) => state.getAllProducts.allProducts);

    useEffect(() => {
        dispatch(fetchProducts())
    }, [])

    return (
        <>
            <main className="main-content">
                <div className="products-wrapper">
                    {products?.map((product) => <Product
                        key={product.id}
                        id={product.id}
                        name={product.name}
                        price={product.price}
                        picture={product.picture}
                        color={product.color}
                        linkPath={`/product/${product.id}`}
                    />)
                    }
                </div>
            </main>
        </>
    );
};

export default Products;