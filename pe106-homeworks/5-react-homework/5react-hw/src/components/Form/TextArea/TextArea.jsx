import {Field, ErrorMessage} from "formik"
import cn from "classnames"
import PropTypes from "prop-types";

import "./TextArea.scss"

import React from 'react';

const TextArea = (props) => {

    const {
        className,
        label,
        name,
        placeholder,
        error,
        rows,
        ...restProps
    } = props

    return (
        <>
            <label className={cn("form-item", className, {"has-validation": error})}>
                <p className="form-label">{label}</p>
                <Field
                    as={"textarea"}
                    rows={rows}
                    className="form-check"
                    name={name}
                    placeholder={placeholder}
                    {...restProps}
                />
                <ErrorMessage name={name} className="error-message" component={"p"} message={error} />
            </label>
        </>
    );
};

TextArea.propTypes = {
    className: PropTypes.string,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    error: PropTypes.bool,
    rows: PropTypes.number,
}

export default TextArea;