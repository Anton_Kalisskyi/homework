import React from 'react';
import {useSelector, useDispatch} from "react-redux";
import {selectFromData} from "../../../store/selectors.js";
import {checkoutFormAction} from "../../../store/slices/checkoutSlice.js";
import {Formik, Form} from "formik"
import Input from "../Input/Input.jsx";
import TextArea from "../TextArea/TextArea.jsx";
import Button from "../../Button/Button.jsx"
import {validation} from "../validation.js";

import "./CheckoutForm.scss";

const CheckoutForm = () => {

    const dispatch = useDispatch();
    const formData = useSelector(selectFromData)
    const checkoutProducts = useSelector(state => state.cart.cartProducts);

    return (
        <Formik
            initialValues={formData}
            validationSchema={validation}
            onSubmit={(values, {resetForm}) => {
                dispatch(checkoutFormAction(values));
                const buyerInfo = values;
                const products = checkoutProducts;
                const order = {products, buyerInfo}
                console.log("order", order);
                localStorage.removeItem('cartProducts');
                // window.location.reload();
                resetForm(formData);
            }}
        >
            {({errors, touched}) => (
                <Form className="checkout-form">
                    <fieldset className="form-block">
                        <legend>Buyer</legend>
                        <div className="row">
                            <div className="column">
                                <Input
                                    type="text"
                                    className="form-input"
                                    label={"Name"}
                                    name={"name"}
                                    placeholder={"Name"}
                                    error={errors.name && touched.name}
                                />
                                <Input
                                    type="text"
                                    className="form-input"
                                    label={"Surname"}
                                    name={"surname"}
                                    placeholder={"Surname"}
                                    error={errors.surname && touched.surname}
                                />
                                <Input
                                    type="text"
                                    className="form-input"
                                    label={"Age"}
                                    name={"age"}
                                    placeholder={"Age"}
                                    error={errors.age && touched.age}
                                />
                                <Input
                                    type="text"
                                    className="form-input"
                                    label={"Phone"}
                                    name={"phone"}
                                    placeholder={"Phone"}
                                    error={errors.phone && touched.phone}
                                />
                                <Input
                                    type="text"
                                    className="form-input"
                                    label={"Email"}
                                    name={"email"}
                                    placeholder={"Email"}
                                    error={errors.email && touched.email}
                                />

                            </div>
                        </div>
                    </fieldset>
                    <fieldset className="form-block">
                        <legend>Address</legend>
                        <div className="row">
                            <div className="column">
                                <Input
                                    type="text"
                                    className="form-input"
                                    label={"City"}
                                    name={"city"}
                                    placeholder={"City"}
                                    error={errors.city && touched.city}
                                />
                                <Input
                                    type="text"
                                    className="form-input"
                                    label={"Address"}
                                    name={"address"}
                                    placeholder={"Address"}
                                    error={errors.address && touched.address}
                                />
                            </div>
                        </div>
                    </fieldset>
                    <fieldset className="form-block">
                        <legend>Comment</legend>
                        <div className="row">
                            <div className="column">
                                <TextArea
                                    label={"Comment"}
                                    name={"comment"}
                                    rows={6}
                                    placeholder={"leave me a comment ;)"}
                                    error={errors.comment && touched.comment}
                                />
                            </div>
                        </div>
                    </fieldset>
                    <div>
                        <Button type={"submit"} className="checkout-btn" text="Checkout">
                            Checkout
                        </Button>
                    </div>
                </Form>
            )}
        </Formik>
    );
};

export default CheckoutForm;