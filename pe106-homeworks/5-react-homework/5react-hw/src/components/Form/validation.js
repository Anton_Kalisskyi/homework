import * as yup from "yup"
import InputMaskBox from "./InputMask/InputMask.jsx";

export const validation = yup.object({
    name: yup
        .string()
        .required()
        .min(2, 'Name is to short')
        .max(30, 'Name is to long')
        .matches(/[a-zA-Z]/, 'Only letters'),
    surname: yup
        .string()
        .required()
        .min(2, 'Surname is to short')
        .max(30, 'Surname is to long')
        .matches(/[a-zA-Z]/, 'Only letters'),
    email: yup
        .string()
        .email()
        .required()
        .matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, "email is not valid"),
    phone: yup
        .string()
        .required()
        .matches(/^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/, 'Phone number is not valid')
        .min(10, "too short")
        .max(10, "too long"),

    city: yup
        .string()
        .required()
        .min(2, 'City name is to short')
        .max(30, 'City name is to long')
        .matches(/[a-zA-Z]/, 'Only letters'),
    address: yup
        .string()
        .required()
        .min(2, 'Address is to short'),
    comment: yup
        .string()
})