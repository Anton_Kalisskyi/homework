import {Field, ErrorMessage} from "formik"
import cn from "classnames"
import PropTypes from "prop-types";

import "./Input.scss"

import React from 'react';

const Input = (props) => {

    const {
        className,
        type= "text",
        label,
        name,
        placeholder,
        error,
        ...restProps
    } = props

    return (
        <>
            <label className={cn("form-item", className, {"has-validation": error})}>
                <p className="form-label">{label}</p>
                <Field
                    type={type}
                    className="form-check"
                    name={name}
                    placeholder={placeholder}
                    {...restProps}
                />
                <ErrorMessage name={name} className="error-message" component={"p"} message={error} />
            </label>
        </>
    );
};

// Input.defaultProps = {
//     type: "text",
// }

Input.propTypes = {
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    error: PropTypes.bool,
}

export default Input;