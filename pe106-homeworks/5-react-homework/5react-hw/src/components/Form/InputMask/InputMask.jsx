import {InputMask} from "@react-input/mask"
import cn from "classnames";
import {ErrorMessage, Field} from "formik";
import PropTypes from "prop-types";
import React from "react";

const InputMaskBox = (props) => {

    const {
        className,
        type= "text",
        label,
        name,
        placeholder,
        error,
        ...restProps
    } = props

    return (
        <>
            <label className={cn("form-item", className, {"has-validation": error})}>
                <p className="form-label">{label}</p>
                <InputMask
                    type={type}
                    className="form-check"
                    name={name}
                    placeholder={placeholder}
                    {...restProps}
                />
                <ErrorMessage name={name} className="error-message" component={"p"} message={error} />
            </label>
        </>
    );
};

// Input.defaultProps = {
//     type: "text",
// }

InputMaskBox.propTypes = {
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    error: PropTypes.bool,
}

export default InputMaskBox;