import React from 'react';
import PropTypes from "prop-types";
import {toggleModalAction} from "../../store/slices/modalSlice.js"
import {useDispatch} from "react-redux";

import "./Modal.scss"

const ModalWrapper = ({children}) => {

    const dispatch = useDispatch();
    // const isOpen = useSelector((state) => state.modal.modalOpen);

    return (
       <>
           {/*{*/}
           {/*    isOpen && (*/}
           {/*        <div className="modal-wrapper" onClick={(e) => {*/}
           {/*            if (e.target.classList.contains("modal-wrapper")) {*/}
           {/*                dispatch(toggleModalAction())*/}
           {/*                // onClose();*/}
           {/*            }*/}
           {/*        }}>*/}
           {/*            {children}*/}
           {/*        </div>*/}
           {/*    )*/}
           {/*}*/}


                   <div className="modal-wrapper" onClick={(e) => {
                       if (e.target.classList.contains("modal-wrapper")) {
                           dispatch(toggleModalAction())
                       }
                   }}>
                       {children}
                   </div>

       </>
    );
};

ModalWrapper.propTypes = {
    // close: PropTypes.func,
    // isOpen: PropTypes.bool,
    children: PropTypes.any,
}

export default ModalWrapper;