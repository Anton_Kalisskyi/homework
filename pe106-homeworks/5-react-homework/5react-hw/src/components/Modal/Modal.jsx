import React from 'react';
import PropTypes from "prop-types";

import ModalWrapper from "./ModalWrapper.jsx";
import ModalBox from "./ModalBox.jsx";
import ModalClose from "./ModalClose.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalBody from "./ModalBody.jsx";
import ModalFooter from "./ModalFooter.jsx";

const Modal = ({children}) => {
    return (
            <ModalWrapper>
                <ModalBox>
                    <ModalClose/>
                    <ModalHeader>{children}</ModalHeader>
                    <ModalBody >{children}</ModalBody>
                    <ModalFooter>{children}</ModalFooter>
                </ModalBox>
            </ModalWrapper>
    );
};

Modal.propTypes = {
    children: PropTypes.any,
}

export default Modal;