import React from 'react';
import PropTypes from "prop-types";

const ModalHeader = ({title}) => {

    return (
        <div className="modal-header">
            <span>{title}</span>
        </div>
    );
};

ModalHeader.propTypes = {
    title: PropTypes.string,
}

export default ModalHeader;