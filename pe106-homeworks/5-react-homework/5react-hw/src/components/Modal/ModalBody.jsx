import React from 'react';
import {useSelector} from "react-redux"

const ModalBody = () => {

    const modalProduct = useSelector(state => state.modal.modalContent)

    return (
            <div className="modal-body">
                <img src={modalProduct.picture} alt={modalProduct.name}/>
                <p>{modalProduct.name}</p>
            </div>
    );
};

export default ModalBody;