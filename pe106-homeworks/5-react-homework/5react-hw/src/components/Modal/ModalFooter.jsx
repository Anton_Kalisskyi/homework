import React from 'react';

import Button from "../Button/Button.jsx";
import {toggleModalAction} from "../../store/slices/modalSlice.js"
import {toggleCartProductsAction} from "../../store/slices/cartSlice.js"
import {useSelector, useDispatch} from "react-redux";

const ModalFooter = () => {

    const dispatch = useDispatch();
    const modalProduct = useSelector(state => state.modal.modalContent)

        return (
            <div className="modal-footer">
                <div className="btn-wrapper">
                    <Button type="button"
                            onClick={() => {
                                dispatch(toggleCartProductsAction(modalProduct))
                                dispatch(toggleModalAction())
                                }
                            }
                            className="modal-btn"
                            text="Confirm"
                    />
                    <Button onClick={()=> {dispatch(toggleModalAction())}} type="button" className="modal-btn" text="Cancel"/>
                </div>
            </div>
        );
};

export default ModalFooter;