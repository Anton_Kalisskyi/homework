import React from 'react';
import Products from "../../components/Products/Products.jsx";

const Homepage = () => {
    return (
        <div>
            <Products/>
        </div>
    );
};

export default Homepage;