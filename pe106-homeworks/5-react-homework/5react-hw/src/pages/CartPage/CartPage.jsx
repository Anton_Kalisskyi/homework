import React from 'react';
import Product from "../../components/Products/components/Product/Product.jsx";
import {useSelector} from "react-redux";
import CheckoutForm from "../../components/Form/CheckoutForm/CheckoutForm.jsx";

import "./CartPage.scss"


const CartPage = () => {

    const cartProducts = useSelector(state => state.cart.cartProducts);


    return (
        <>
            <div className="cart-products-wrapper">
                {cartProducts?.map((cartProduct) => <Product
                    key={cartProduct.id}
                    id={cartProduct.id}
                    name={cartProduct.name}
                    price={cartProduct.price}
                    picture={cartProduct.picture}
                    color={cartProduct.color}
                    linkPath={`/product/${cartProduct.id}`}
                />)
                }
            </div>

            <CheckoutForm/>
        </>
    );
};

export default CartPage;