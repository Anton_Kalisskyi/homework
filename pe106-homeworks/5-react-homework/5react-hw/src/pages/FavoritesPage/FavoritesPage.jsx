import React from 'react';
import Product from "../../components/Products/components/Product/Product.jsx";
import {useSelector} from "react-redux"

import "./FavoritesPage.scss"

const FavoritesPage = () => {

    const favoriteProducts = useSelector(state => state.favorites.favoriteProducts);

    return (
        <>
            <div className="wishlist-wrapper">
                {favoriteProducts?.map((favoriteProduct) => <Product
                    key={favoriteProduct.id}
                    id={favoriteProduct.id}
                    name={favoriteProduct.name}
                    price={favoriteProduct.price}
                    picture={favoriteProduct.picture}
                    color={favoriteProduct.color}
                    linkPath={`/product/${favoriteProduct.id}`}
                />)
                }
            </div>
    </>
    );
};

export default FavoritesPage;