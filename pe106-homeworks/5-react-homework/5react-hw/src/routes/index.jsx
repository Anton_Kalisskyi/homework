import {Routes, Route} from "react-router-dom"
import Homepage from "../pages/HomePage/Homepage.jsx";
import NotPage from "../pages/NotPage/NotPage.jsx";
import CartPage from "../pages/CartPage/CartPage.jsx";
import FavoritesPage from "../pages/FavoritesPage/FavoritesPage.jsx";
import ProductPage from "../pages/ProductPage/ProductPage.jsx";

export default () => {
    return(
            <Routes>
                <Route path={"/"} element={<Homepage/>}/>
                <Route path={"/cart"} element={<CartPage/>} />
                <Route path={"/wishlist"} element={<FavoritesPage/>} />
                <Route path={"/product/:id"} element={<ProductPage/>}/>
                <Route path={"*"} element={<NotPage/>} />
            </Routes>
    )
}