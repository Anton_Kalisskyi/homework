import React from 'react'
import Header from "./components/Header/Header.jsx";
import AppRoutes from "./routes"
import ProductModalAddToCart from "./components/Products/components/ProductModals/ProductModalAddToCart.jsx";
import {useSelector} from "react-redux";

import './App.scss'

function App() {

    const modalOpen = useSelector(state => state.modal.modalOpen)

    return (
        <>
            <Header/>
            <AppRoutes/>
            { modalOpen && (<ProductModalAddToCart/>) }
        </>
    )
}

export default App
