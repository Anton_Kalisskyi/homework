import {createSlice} from '@reduxjs/toolkit';

const initialState = {
    checkoutFormData: {
        orderInfo: {
            name: "",
            surname: "",
            age: "",
            phone: "",
            email: "",
            city: "",
            address: "",
            comment: "",
        }
    }
}

const checkoutSlice = createSlice ({
    name: "checkoutForm",
    initialState,
    reducers: {
        checkoutForm: (state, payload) => {
            state.orderInfo = payload
        }
    }
})

export const {checkoutForm: checkoutFormAction} = checkoutSlice.actions
export default checkoutSlice.reducer