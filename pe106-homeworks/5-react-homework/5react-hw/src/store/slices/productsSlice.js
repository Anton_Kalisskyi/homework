import {createSlice, createAsyncThunk} from "@reduxjs/toolkit"
import {sendRequest} from "../../helpers/sendRequest.js"

const initialState = {
    allProducts: [],
}

export const fetchProducts = createAsyncThunk(
    'products/fetchProducts',
    async () => {
        const allData = await sendRequest('/products.json');
        return allData
    }
)

const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        getAllProducts: (state, {payload}) => {
            state.allProducts = [...payload]
        },
    },
        extraReducers: (builder) => {
        builder.addCase(fetchProducts.fulfilled, (state, {payload}) => {
            state.allProducts = [...payload]
        })
    }
})



export const {getAllProducts: getAllProductsAction} = productsSlice.actions
export default productsSlice.reducer


