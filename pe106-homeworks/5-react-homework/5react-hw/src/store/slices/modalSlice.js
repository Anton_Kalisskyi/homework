import {createSlice} from "@reduxjs/toolkit"

const initialState = {
    modalOpen: false,
    modalContent: {name: null, picture: null, id: null, price: null, color: null, linkPath: null}
}

const modalSlice = createSlice({
    name: "modal",
    initialState,
    reducers: {
        toggleModal: (state, payload) => {
            state.modalOpen = !state.modalOpen
        },
        updateModalContent: (state, {payload}) => {
            state.modalContent = {...payload}
        }
    }
})

export const {toggleModal: toggleModalAction, updateModalContent: updateModalContentAction} = modalSlice.actions;
export default modalSlice.reducer