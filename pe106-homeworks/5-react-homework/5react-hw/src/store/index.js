import {combineReducers, configureStore} from "@reduxjs/toolkit"
import favoriteReducer from "./slices/favoritesSlice.js"
import cartReducer from "./slices/cartSlice.js"
import modalReducer from "./slices/modalSlice.js"
import productsReducer from "./slices/productsSlice.js"
import checkoutReducer from "./slices/checkoutSlice.js"

const rootReducer = combineReducers({
    favorites: favoriteReducer,
    cart: cartReducer,
    modal: modalReducer,
    getAllProducts: productsReducer,
    checkoutForm: checkoutReducer,
})

export const store = configureStore({
    reducer: rootReducer
})