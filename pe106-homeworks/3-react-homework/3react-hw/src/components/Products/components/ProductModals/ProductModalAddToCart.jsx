import React from "react";
import PropTypes from "prop-types";

import ModalWrapper from "../../../Modal/ModalWrapper.jsx";
import ModalBox from "../../../Modal/ModalBox.jsx";
import ModalClose from "../../../Modal/ModalClose.jsx";
import ModalHeader from "../../../Modal/ModalHeader.jsx";
import ModalBody from "../../../Modal/ModalBody.jsx";
import ModalFooter from "../../../Modal/ModalFooter.jsx";


const ProductModalAddToCart = ({onClose, onCart, children, isOpen, name, picture, cartProducts, id}) => {

    const isAddedToCartTrue = cartProducts.some((item) => item.id === id)

    return (
        <ModalWrapper onClose={onClose} isOpen={isOpen}>
            <ModalBox>
                <ModalClose onClose={onClose}/>
                <ModalHeader title={isAddedToCartTrue ? "Remove this product from the cart?" : "Add this product to the cart?"}/>
                <ModalBody name={name} picture={picture}>{children}</ModalBody>
                <ModalFooter onClose={onClose} onCart={onCart}>{children}</ModalFooter>
            </ModalBox>
        </ModalWrapper>
    );
};

ProductModalAddToCart.propTypes = {
    onClose: PropTypes.func,
    onCart: PropTypes.func,
    isOpen: PropTypes.bool,
    children: PropTypes.any,
    name: PropTypes.string,
    picture: PropTypes.string,
}

export default ProductModalAddToCart;