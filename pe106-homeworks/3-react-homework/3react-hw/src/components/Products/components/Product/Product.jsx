import React, {useState} from 'react';
import Button from "../../../Button/Button.jsx";
import FavoritesIcon from "../../../ui/icons/FavoritesIcon.jsx";

import cn from 'classnames'
import "./Product.scss"
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

const Product = (props) => {

    const {
        id,
        name,
        price,
        picture,
        color,
        onClick,
        onFavorite,
        linkPath,
        favoriteProducts,
        cartProducts,
    } = props

    const isFavoriteTrue = favoriteProducts.some((item) => item.id === id)
    const isAddedToCartTrue = cartProducts.some((item) => item.id === id)

    const [isFavorite, setIsFavorite] = useState(isFavoriteTrue)
    const [isAddedToCart, setIsAddedToCart] = useState(isAddedToCartTrue)

    const handleFavorite = () => {
        setIsFavorite(!isFavorite)
    }

    const handleCartProduct = () => {
        setIsAddedToCart(!isAddedToCart)
    }

    return (
        <div className="product">
            <div className="product-icons-wrapper" onClick={()=> {
                handleFavorite()
                onFavorite()
                }
            }
            >
                <FavoritesIcon className={cn("product-icon", {"is-favorite" : isFavorite})}/>
            </div>
            <Link to={linkPath} className="product-link">
            <div className="product-image">
                <img
                    src={picture}
                    alt={name}/>
            </div>
            </Link>
            <div className="product-details">
                <Link to={linkPath} className="product-link">
                <h3 className="product-name">{name}</h3>
                </Link>
                <span className="product-color">{`color: ${color}`}</span>
                <span className="product-ia">{`id: ${id}`}</span>
            </div>
            <div className="btn-wrapper">
                <Button
                    type="button"
                    className={isAddedToCartTrue ? "product-btn_painted" : "product-btn"}
                    onClick={() => {
                        onClick()
                        handleCartProduct()
                }
                }
                text={isAddedToCartTrue ? "In the cart" : "Add to cart"}></Button>
                <p className="product-price">{`$${price}`}</p>
            </div>
        </div>
    );
};

Product.propTypes = {
    id: PropTypes.number,
    price: PropTypes.number,
    name: PropTypes.string,
    picture: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func,
    onFavorite: PropTypes.func,

}

export default Product;