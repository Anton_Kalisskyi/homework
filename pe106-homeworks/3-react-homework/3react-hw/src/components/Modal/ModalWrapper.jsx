import React from 'react';
import PropTypes from "prop-types";

import "./Modal.scss"

const ModalWrapper = ({children, onClose, isOpen}) => {
    return (
       <>
           {
               isOpen && (
                   <div className="modal-wrapper" onClick={(e) => {
                       if (e.target.classList.contains("modal-wrapper")) {
                           onClose();
                       }
                   }}>
                       {children}
                   </div>
               )
           }
       </>
    );
};

ModalWrapper.propTypes = {
    close: PropTypes.func,
    isOpen: PropTypes.bool,
    children: PropTypes.any,
}

export default ModalWrapper;