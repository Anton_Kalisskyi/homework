import React, {useState, useEffect} from 'react'
import Header from "./components/Header/Header.jsx";
import AppRoutes from "./routes"
import './App.scss'

function App() {

    const localStorageFavorites = JSON.parse(localStorage.getItem('favoriteProducts')) === null ? [] : JSON.parse(localStorage.getItem('favoriteProducts'))
    const localStorageCart = JSON.parse(localStorage.getItem('cartProducts')) === null ? [] : JSON.parse(localStorage.getItem('cartProducts'))

    const [favoriteProducts, setFavoriteProducts] = useState(localStorageFavorites)
    const [cartProducts, setCartProducts] = useState(localStorageCart)
    const [currentProduct, setCurrentProduct] = useState({})

    const handleCurrentProduct = (product) => {
        return setCurrentProduct(product)
    }

    useEffect(() => {
        const favoriteProducts = JSON.parse(localStorage.getItem('favoriteProducts'));
        if (favoriteProducts) {
            setFavoriteProducts(favoriteProducts);
        }
    }, []);

    useEffect(() => {
        const cartProducts = JSON.parse(localStorage.getItem('cartProducts'));
        if (cartProducts) {
            setCartProducts(cartProducts);
        }
    }, []);

    useEffect(() => {
        localStorage.setItem('favoriteProducts', JSON.stringify(favoriteProducts));
    }, [favoriteProducts]);

    useEffect(() => {
        localStorage.setItem('cartProducts', JSON.stringify(cartProducts));
    }, [cartProducts]);

    const handleFavoriteProducts = (product) => {
        if (favoriteProducts.length >= 1) {
            let isProductAdded = favoriteProducts.some(({id}) => id === product.id)
            if (!isProductAdded) {
                setFavoriteProducts((prevState) => [...prevState, product])
            }
            if (isProductAdded) {
                let indexOfAddedProduct = favoriteProducts.findIndex(({id}) => id === product.id)
                if (indexOfAddedProduct !== -1) {
                    let changedFavoriteProducts = favoriteProducts.splice(indexOfAddedProduct, 1)
                    setFavoriteProducts((changedFavoriteProducts) => [...changedFavoriteProducts])
                    localStorage.removeItem("favoriteProducts");
                }
            }
        } else {
            setFavoriteProducts((prevState) => [...prevState, product])
        }
    }

    console.log("favoriteProducts", favoriteProducts);
    console.log("cartProducts", cartProducts);

    const handleCartProducts = (product) => {
        if (cartProducts.length >= 1) {
            let isAdded = cartProducts.some(({id}) => id === product.id)
            if (!isAdded) {
                setCartProducts((prevState) => [...prevState, product])
            }
            if (isAdded) {
                let indexOfAdded = cartProducts.findIndex(({id}) => id === product.id)
                if (indexOfAdded !== -1) {
                    let changedCartProducts = cartProducts.splice(indexOfAdded, 1)
                    setCartProducts((changedCartProducts) => [...changedCartProducts])
                    localStorage.removeItem("cartProducts");
                }
            }
        } else {
            setCartProducts((prevState) => [...prevState, product])
        }

    }

  return (
      <>
          <Header
              favoriteProductsCounter={favoriteProducts.length}
              cartProductsCounter={cartProducts.length}
          />
          <AppRoutes
              onFavorite={handleFavoriteProducts}
              onCart={handleCartProducts}
              favoriteProducts={favoriteProducts}
              cartProducts={cartProducts}
              currentProduct={currentProduct}
              handleCurrentProduct={handleCurrentProduct}
          />
      </>
  )
}

export default App
